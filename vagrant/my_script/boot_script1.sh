#!/bin/bash

echo "127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
127.0.1.1 node1.tp2.b2
192.168.1.12 node2.tp2.b2" > "/etc/hosts"

# User
useradd admin -m
usermod -aG wheel admin

#Firewall
systemctl start firewalld
firewall-cmd --add-port=80/tcp --permanent
firewall-cmd --add-port=443/tcp --permanent
firewall-cmd --reload

# Selinux
setenforce 0
echo "SELINUX=permissive\nSELINUXTYPE=targeted" > "/etc/selinux/config"

# install
yum update -y
yum install -y epel-release
yum install -y nginx

# user
useradd nginx -M -s /sbin/nologin

## keys
mv ./server.key /etc/pki/tls/private/node1.tp2.b2.key
chmod 400 /etc/pki/tls/private/node1.tp2.b2.key
chown nginx:nginx /etc/pki/tls/private/node1.tp2.b2.key

mv ./server.crt /etc/pki/tls/certs/node1.tp2.b2.crt
chmod 444 /etc/pki/tls/certs/node1.tp2.b2.crt
chown nginx:nginx /etc/pki/tls/certs/node1.tp2.b2.crt

# Site
mkdir /srv/site{1,2}
echo "<h1>HELLO FROM SITE 1</h1>" > /srv/site1/index.html
echo "<h1>HELLO FROM SITE 2</h1>" > /srv/site2/index.html

chown nginx:nginx /srv/site1 -R
chown nginx:nginx /srv/site2 -R
chmod 700 /srv/site1 /srv/site2
chmod 400 /srv/site1/index.html /srv/site2/index.html

rm /etc/nginx/nginx.conf
cp /vagrant/my_config/nginx.conf /etc/nginx/nginx.conf

systemctl restart nginx