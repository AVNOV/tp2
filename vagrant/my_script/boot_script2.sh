#!/bin/bash

# Hosts
echo "127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
127.0.1.1 node2.tp2.b2
192.168.1.11 node1.tp2.b2" >> "/etc/hosts"

# User
useradd admin -m
usermod -aG wheel admin

#Firewall
systemctl start firewalld
firewall-cmd --add-port=80/tcp --permanent
firewall-cmd --add-port=443/tcp --permanent
firewall-cmd --reload

# Selinux
sudo setenforce 0
echo "SELINUX=permissive\nSELINUXTYPE=targeted" > /etc/selinux/config


yum update -y