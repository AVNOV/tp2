﻿# Prérequis
	
					

												

## I.Déploiement

Configuration du vagrantfile pour une machine virtuelle centos7 avec :

 - 1Go RAM,
 - Ajout d'une IP statique 192.168.2.11/24,
 - Définition d'un nom (interne à Vagrant),
 - Définition d'un hostname.

___

	    - Création d'un répertoire de travail
	
       $ mkdir vagrant
       $ cd vagrant


	    - Initialisez un Vagrantfile
	 
		$ vagrant init centos/7
	    A `Vagrantfile` has been placed in this directory. You are now
	    ready to `vagrant up` your first virtual environment! Please read
	    the comments in the Vagrantfile as well as documentation on
	    `vagrantup.com` for more information on using Vagrant.

 

___
Configuration du vagrantfile pour une génération d'une machine virtuelle unique :
    
    Vagrant.configure("2") do |config|
	    config.vm.box = "centos/7"
	    config.vbguest.auto_update = false
	    config.vm.box_check_update = false
	    config.vm.synced_folder  ".", "/vagrant", disabled:  true
	    config.vm.network  "private_network", ip:  "192.168.2.11", mask:  "255.255.255.0"
	    config.vm.hostname = "node1.tp2.b2"
	    config.vm.provider  :virtualbox  do |vb|
		    vb.customize ["modifyvm", :id, "--memory", "1024"]
		    vb.name = "tp2"
	    end
	    config.vm.provision  "shell", path:  "my_script/install_vim.sh"
	end

___

  - La machine exécute un script shell au démarrage qui install le paquet `vim`  grâce à config.vm.provision.    
    
          #!/bin/bash
        	sudo yum install -y vim-enhanced

___

  - Commandes basiques et intrinsèque à l'utilisation de vagrant :    
    
        $ vagrant plugin install vagrant-vbguest
        $ vagrant up
        [...]
        $ vagrant status
        $ vagrant ssh
        $ vagrant destroy -f
        $ vagrant halt

___

## II.Re-package

Il est possible de packager soi-même une box Vagrant afin d'avoir une VM sur-mesure dès qu'elle s'allume.
On peut la créer depuis le fichier .iso correspondant à l'image officielle d'un OS donné.
Il est aussi possible de la générer à partir d'une box existante.

 - En CLI, ça donne :

    $ vagrant package --output centos7_test.box
    $ vagrant box add centos7_test centos7_test.box
___
Configurations de la machine virtuelle :

    [vagrant@node1 ~]$ sudo setenforce 0
    [vagrant@node1 ~]$ sudo systemctl enable firewalld
    Created symlink from /etc/systemd/system/dbus-org.fedoraproject.FirewallD1.service to /usr/lib/systemd/system/firewalld.service.
    Created symlink from /etc/systemd/system/multi-user.target.wants/firewalld.service to /usr/lib/systemd/system/firewalld.service.
    [vagrant@node1 ~]$ sudo systemctl start firewalld
    [vagrant@node1 ~]$ sudo systemctl status firewalld
    ● firewalld.service - firewalld - dynamic firewall daemon
       Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
       Active: active (running) since Tue 2020-09-29 10:54:41 UTC; 6s ago
         Docs: man:firewalld(1)
     Main PID: 12218 (firewalld)
       CGroup: /system.slice/firewalld.service
               └─12218 /usr/bin/python2 -Es /usr/sbin/firewalld --nofork ...
    
    Sep 29 10:54:41 node1.tp2.b2 systemd[1]: Starting firewalld - dynami....
    Sep 29 10:54:41 node1.tp2.b2 systemd[1]: Started firewalld - dynamic....
    Sep 29 10:54:41 node1.tp2.b2 firewalld[12218]: WARNING: AllowZoneDrif...
    Hint: Some lines were ellipsized, use -l to show in full.
    [vagrant@node1 ~]$ sudo firewall-cmd --add-port=22/tcp --permanent
    success
    [vagrant@node1 ~]$ sudo firewall-cmd --reload
    success
___

## III.Multi-node déploiement

Il est possible de déployer et gérer plusieurs VMs en un seul `Vagrantfile`.

Ci-dessous, la configuration du `vagrantfile` pour une génération de machine virtuelle multiple (Deux vm en l'occurence..) :


    Vagrant.configure("2") do |config|
    
      ## All VM's basic config attribute ##
      config.vm.box = "centos7_test"
      config.vbguest.auto_update = false
      config.vm.box_check_update = false 
      config.vm.synced_folder ".", "/vagrant", disabled: true
    
      ## NODE1 creation / config / ip / mem / name ##
      config.vm.define "node1" do |node1|
        node1.vm.network "private_network", ip: "192.168.2.21", mask: "255.255.255.0"
        node1.vm.hostname = "node1.tp2.b2"
        node1.vm.provider :virtualbox do |vb|
          vb.customize ["modifyvm", :id, "--memory", "1024"]
          vb.name = "node1"
        end
      end
    
      ## NODE2 creation / config / ip / mem / name ##
      config.vm.define "node2" do |node2|
        node2.vm.network "private_network", ip: "192.168.2.22", mask: "255.255.255.0"
        node2.vm.hostname = "node2.tp2.b2"
        node2.vm.provider :virtualbox do |vb|
          vb.customize ["modifyvm", :id, "--memory", "512"]
          vb.name = "node2"
        end
      end
      
    end
___



